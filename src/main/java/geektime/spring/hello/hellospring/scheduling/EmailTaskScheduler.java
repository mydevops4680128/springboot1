package geektime.spring.hello.hellospring.scheduling;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmailTaskScheduler {

    @Autowired
    private CronTaskRegistrar cronTaskRegistrar;

    public void addOrUpdateEmailTask(String email, String region, String period) {
        EmailTask task = new EmailTask(email, region, period);
        cronTaskRegistrar.addCronTask(task, task.getPeriod());
    }

    public void removeEmailTask(String email, String region) {
        EmailTask task = new EmailTask(email, region);
        cronTaskRegistrar.removeCronTask(task);
    }
}
