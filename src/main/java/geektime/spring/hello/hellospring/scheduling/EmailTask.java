package geektime.spring.hello.hellospring.scheduling;

import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.Objects;

public class EmailTask implements Runnable{

    private static final Logger logger = LoggerFactory.getLogger(EmailTask.class);

    private String email;

    private String region;

    @Getter
    private String period;

    public EmailTask(String email, String region) {
        this(email, region, null);
    }

    public EmailTask(String email, String region, String period) {
        this.email = email;
        this.region = region;
        this.period = period;
    }

    @Override
    public void run() {
//        logger.info("Scheduled task start - email: {}, period: {}, region: {}", email, period, region);

        System.out.println("Scheduled task start - email: " + email + ", period: " + period + ", region: " + region);

        long startTime = System.currentTimeMillis();

        JavaMailSender mailSender = SpringContextUtils.getBean(JavaMailSenderImpl.class);
        String from = SpringContextUtils.getPropertyValue("spring.mail.username");

        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(from);
        message.setTo(email);
        message.setSubject("Test Message");
        message.setText("a test mail from springboot mail scheduler.");

        System.out.println("from : " + from);

        if (mailSender == null) {
            System.out.println("MailSender is null.");
        }

        try {
            mailSender.send(message);
            System.out.println("Mail sent ");
        } catch (Exception e){
            System.out.println("failed to send mail! " + e);
        }

        long times = System.currentTimeMillis() - startTime;
//        logger.info("Scheduled task end - email: {}, period: {}, region: {}, time consumed: {}", email, period, region, times);
        System.out.println("Scheduled task end - email: " + email + ", period: " + period + ", region: " + region + ", time consumed: "+ times);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        EmailTask that = (EmailTask) o;
        return email.equals(that.email) && region.equals(that.region);
    }

    public int hashCode() {
        return Objects.hash(email, region);
    }
}
