package geektime.spring.hello.hellospring.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HelloController {

    @RequestMapping("/hihi")
    public String hihi() {
        return "hihi";
    }

}
