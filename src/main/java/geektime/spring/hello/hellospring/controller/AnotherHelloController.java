package geektime.spring.hello.hellospring.controller;

import geektime.spring.hello.hellospring.scheduling.EmailTaskScheduler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

@RestController
public class AnotherHelloController {

    @Autowired
    private EmailTaskScheduler emailTaskScheduler;

    @RequestMapping("/hello")
    public String hello() {
        return "Hello Spring Package";
    }

    @RequestMapping("/world")
    public String world() {
        return "Hello World";
    }

    @RequestMapping(value="/metalocator", produces=MediaType.APPLICATION_XML_VALUE)
    public String metaLocator() {
        RestTemplate restTemplate = new RestTemplate();
        String url = "https://admin.metalocator.com/xmlrpc";

        StringBuilder sb = new StringBuilder();
        sb.append("<?xml version=\"1.0\"?>").append("\n")
                .append("<methodCall>").append("\n")
                .append("<methodName>MetaLocator.exportLocations</methodName>").append("\n")
                .append("<params>").append("\n")
                .append("<param><value><string>haijian@hotmail.com</string></value></param>").append("\n")
                .append("<param><value><string>ask me :)</string></value></param>").append("\n")
                .append("<param><value><string>0</string></value></param>").append("\n")
                .append("<param><value><string>100</string></value></param>").append("\n")
                .append("</params>").append("\n")
                .append("</methodCall>");

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_XML);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_XML));
        HttpEntity<String> httpEntity = new HttpEntity<>(sb.toString(), headers);

        ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST, httpEntity, String.class);

        return responseEntity.getBody();
    }


    @RequestMapping("/addMail")
    public String addEmailToMailchimp() {

        return "";
    }
}
