package geektime.spring.hello.hellospring.controller;

import geektime.spring.hello.hellospring.scheduling.EmailTaskScheduler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MailAPIController {

    @Autowired
    private EmailTaskScheduler emailTaskScheduler;

    @PutMapping("/api/schedule/email")
    public void scheduleMailAdd(@RequestParam(name = "to") String email, @RequestParam String period, @RequestParam String region) {
        System.out.println("in scheduleMailAdd() ...");
        System.out.println("email : " + email);
        System.out.println("period : " + period);
        System.out.println("region : " + region);

        emailTaskScheduler.addOrUpdateEmailTask(email, region, period);
    }

    @PatchMapping("/api/schedule/email")
    public void scheduleMailUpdate(@RequestParam(name = "to") String email, @RequestParam String period, @RequestParam String region) {
        System.out.println("in scheduleMailUpdate() ...");
        System.out.println("email : " + email);
        System.out.println("period : " + period);
        System.out.println("region : " + region);

        emailTaskScheduler.addOrUpdateEmailTask(email, region, period);
    }

    @DeleteMapping("/api/schedule/email")
    public void scheduleMailRemove(@RequestParam(name = "to") String email, @RequestParam String region) {
        System.out.println("in scheduleMailRemove() ...");
        System.out.println("email : " + email);
        System.out.println("region : " + region);

        emailTaskScheduler.removeEmailTask(email, region);
    }
}
