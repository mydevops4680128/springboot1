#!/bin/bash

export DOCKER_HOST=tcp://172.17.0.1:2375

container_name="mytest"
image_name="mytest"

if [ "$(docker inspect -f '{{.State.Running}}' $container_name)" = "true" ]; then
  docker stop $container_name
fi

if [ "$(docker ps -aq -f name=$container_name)" ]; then
  docker rm $container_name
fi

if [ "$(docker images -q $image_name)" ]; then
  docker rmi $image_name
fi

docker build -t mytest .
docker run -d -p 8082:8080 --name mytest mytest